﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login2
{
  public class ExamInfo
    {
        private string eID;
        private string eName;
        private string eGroup;
        private string eDayGK;
        private string eTimeGK;
        private string eRoomGK;
        private string eDayCK;
        private string eTimeCK;
        private string eRoomCK;

        public string EID { get => eID; set => eID = value; }
        public string EName { get => eName; set => eName = value; }
        public string EGroup { get => eGroup; set => eGroup = value; }
        public string EDayGK { get => eDayGK; set => eDayGK = value; }
        public string ERoomGK { get => eRoomGK; set => eRoomGK = value; }
        public string EDayCK { get => eDayCK; set => eDayCK = value; }
        public string ERoomCK { get => eRoomCK; set => eRoomCK = value; }
        public string ETimeGK { get => eTimeGK; set => eTimeGK = value; }
        public string ETimeCK { get => eTimeCK; set => eTimeCK = value; }
    }
}

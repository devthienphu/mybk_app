﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using xNet;

namespace Login2
{
    /// <summary>
    /// Interaction logic for ChangeByMail.xaml
    /// </summary>
    public partial class ChangeByMail : Window
    {
        public ChangeByMail()
        {
            InitializeComponent();
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HttpRequest http = new HttpRequest();
            string data = "login="+user.Text+"&mail="+mail.Text;
            http.Post("https://account.hcmut.edu.vn/index.php?action=sendtoken", data, "application/x-www-form-urlencoded");
            MessageBox.Show("Please check your email!");
        }

        private void ChangeMail_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using xNet;

namespace Login2
{
    /// <summary>
    /// Interaction logic for ChangePass.xaml
    /// </summary>
    public partial class ChangePass : Window
    {
        public ChangePass()
        {
            InitializeComponent();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            if (newPass.Password != confirm.Password)
            {
                MessageBox.Show("Wrong confirm password");
            }
            else {
                MessageBox.Show("Are you sure?");
                HttpRequest http = new HttpRequest();
                string data = "login=" + user.Text + "&oldpassword=" + oldPass.Password.ToString() + "&newpassword=" + newPass.Password.ToString() + "&confirmpassword=" + confirm.Password.ToString() + "";
                http.Post("https://account.hcmut.edu.vn/index.php", data, "application/x-www-form-urlencoded");
                MessageBox.Show("Post succeess - Please close app and login again");
            }
           
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ChangePassControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}

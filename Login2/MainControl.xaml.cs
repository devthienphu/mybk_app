﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using xNet;
using CefSharp;

namespace Login2
{
    /// <summary>
    /// Interaction logic for MainControl.xaml
    /// </summary>
    public partial class MainControl : Window
    {

        #region method
        static string user = ((MainWindow)Application.Current.MainWindow).email.Text;
        static string pass= ((MainWindow)Application.Current.MainWindow).pass.Password.ToString();
        static string score = ((MainWindow)Application.Current.MainWindow).diem.Text;
        static string name = ((MainWindow)Application.Current.MainWindow).iname.Text;
        static string group = ((MainWindow)Application.Current.MainWindow).igroup.Text;



        ObservableCollection<CourseInfo> listCourse;
        ObservableCollection<EventsCourse> eventsList;
        public ObservableCollection<CourseInfo> ListCourse { get => listCourse; set => listCourse = value; }
        public ObservableCollection<EventsCourse> EventsList { get => eventsList; set => eventsList = value; }

        #endregion

        public MainControl()
        {
            InitializeComponent();
           

            iname.Text = name;          // get tên
            igroup.Text = group;        // get Khoa

            CourseWeb.backToMain += CourseWeb_backToMain;

            ListCourse = new ObservableCollection<CourseInfo>();
            EventsList = new ObservableCollection<EventsCourse>();

            Task thrdSubject = new Task(CrawlCourse);
            thrdSubject.Start();


            Task thrdScore = new Task(ScoreHeader);
            thrdScore.Start();

            Task thrdCovid = new Task(CovidUpdate);
            thrdCovid.Start();
        }

        // event back to main 
        private void CourseWeb_backToMain(object sender, EventArgs e)
        {
            CourseWeb.Visibility = Visibility.Hidden;
            MainC.Visibility = Visibility.Visible;
        }

        void ScoreHeader()
        {
            JObject jsScore = JObject.Parse(score);

            string tbtl = "null";
            string tctl = "null";


            if (jsScore.Count == 1)
            {

                string aScore = "{" + jsScore.First.ToString() + "}";

                JObject aScorejs = JObject.Parse(aScore);
                string tempScore = aScorejs.First.First.ToString();

                JObject tempScoreCv = JObject.Parse(tempScore);

                tbtl = tempScoreCv["diem_tbtl"].ToString();
                tctl = tempScoreCv["so_tctl"].ToString();
            }

            if (jsScore.Count > 1)
            {
                string aScore = "{" + jsScore.First.Next.ToString() + "}";

                JObject aScorejs = JObject.Parse(aScore);
                string tempScore = aScorejs.First.First.ToString();

                JObject tempScoreCv = JObject.Parse(tempScore);

                tbtl = tempScoreCv["diem_tbtl"].ToString();
                tctl = tempScoreCv["so_tctl"].ToString();
            }


            Application.Current.Dispatcher.Invoke(new Action(() => {
                avgScore.Text = tbtl;
                totalCre.Text = tctl;
            }));
            

        }

        //complete
        void CrawlCourse()
        {

            #region Post login       

            #region add cookie
            HttpRequest httpLogin = new HttpRequest();
            httpLogin.Cookies = new CookieDictionary();
            var cookies = httpLogin.Get("https://sso.hcmut.edu.vn/cas/login").Cookies;
            httpLogin.Cookies.Add("_ga", "GA1.3.1366266226.1612606482");
            #endregion

            #region Post Login
            string html = httpLogin.Get("https://sso.hcmut.edu.vn/cas/login").ToString();
            string pattern = @"LT-(.*?)""";
            string tokenLT = Regex.Match(html, pattern, RegexOptions.Singleline).Value.Replace(@"""", "");
            string data = @"username=" + user+ "&password=" + pass + "&lt=" + tokenLT + "&execution=e2s1&_eventId=submit&submit=Login";
            html = httpLogin.Post("https://sso.hcmut.edu.vn/cas/login?service=http%3A%2F%2Fmybk.hcmut.edu.vn%2Fstinfo%2F", data, "application/x-www-form-urlencoded").ToString();
            html = httpLogin.Get("https://sso.hcmut.edu.vn/cas/login?service=http%3A%2F%2Fe-learning.hcmut.edu.vn%2Flogin%2Findex.php%3FauthCAS%3DCAS").ToString();
            #endregion


            #endregion


            #region Get Json data Subject

            #region token
             html = httpLogin.Get("http://e-learning.hcmut.edu.vn/my/").ToString();


            Task eventList = new Task(() => {
                 pattern = @"#event_(.*?)<";

                var list = Regex.Matches(html, pattern, RegexOptions.Singleline);

                for (int i = 0; i < list.Count; i++)
                {
                    string ev = list[i].ToString();
                    int index = ev.IndexOf(">");
                    ev = ev.Remove(0, index+1);
                    ev = ev.Remove(ev.Length - 1, 1);

                    Application.Current.Dispatcher.Invoke(new Action(() => {
                        EventsList.Add(new EventsCourse { Events = ev });
                        EventList.ItemsSource = EventsList;
                    }));
                  
                }
            });
            eventList.Start();



            string sesskey = Regex.Match(html, @"sesskey"":""(.*?)""", RegexOptions.Singleline).Value.Replace(@"sesskey"":""", "").Replace(@"""","");

            #endregion

           

            data = @"[{""index"":0,""methodname"":""core_course_get_enrolled_courses_by_timeline_classification"",""args"":{""offset"":0,""limit"":0,""classification"":""allincludinghidden"",""sort"":""id desc"",""customfieldname"":"""",""customfieldvalue"":""""}}]";

            string listSubject = httpLogin.Post("http://e-learning.hcmut.edu.vn/lib/ajax/service.php?sesskey="+sesskey+"&info=core_course_get_enrolled_courses_by_timeline_classification", data, "application/json; charset=utf-8").ToString();
           

            #region Json convert

            string convert = listSubject.Remove(0, 1);      // remove[
            convert= convert.Remove(convert.Length-1, 1); //remove ]
            
            JObject jconvert = JObject.Parse(convert);
            string jsonName = jconvert["data"].ToString();// select data

            JObject jNameArray = JObject.Parse(jsonName);

            #endregion

            #endregion

            #region AddCourse

            int count = jNameArray.First.First.Count();

            for(int i = 0; i < count; i++)
            {
                string nameArray = jNameArray.First.First[i].ToString();

                JObject jname = JObject.Parse(nameArray);

                string name = jname["fullname"].ToString(); 
                string url = jname["viewurl"].ToString();

                Application.Current.Dispatcher.Invoke(new Action(() => {
                    ListCourse.Add(new CourseInfo { CourseName = name, Url = url, UserName=user,PassWord=pass });      //accpect live view            
                    Course.ItemsSource = ListCourse;
                }));
            }

            #endregion

        }
            
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Cef.Shutdown();
            Close();
            Environment.Exit(0);


        }

        private void MenuButtons_GotFocus(object sender, RoutedEventArgs e)
        {

            Schedule schedule = new Schedule();
            schedule.Show();

        }

        private void ScoreTable_GotFocus(object sender, RoutedEventArgs e)
        {
            TestScore scoreList = new TestScore();
            scoreList.Show();
        }

        private void TestExam_GotFocus(object sender, RoutedEventArgs e)
        {
            Exam exam = new Exam();
            exam.Show();
        }

        private void Btn_Course_Click(object sender, RoutedEventArgs e)
        {

            CourseInfo courseSender = (sender as Button).DataContext as CourseInfo;

            CourseWeb.Visibility = Visibility.Visible;
            MainC.Visibility = Visibility.Hidden;

            CourseWeb.CourseUrl = courseSender;
        }


        void CovidUpdate()
        {
            HttpRequest httpCovid = new HttpRequest();
            string html = httpCovid.Get("https://ncov.moh.gov.vn/trang-chu").ToString();

            string pattern = @"font24"">(.*?)<";

            var listData = Regex.Matches(html, pattern, RegexOptions.Singleline);

            string vcase = listData[0].ToString().Replace(@"font24"">", "").Replace("<", "");
            string vtreatment = listData[1].ToString().Replace(@"font24"">", "").Replace("<", "");
            string vfine = listData[2].ToString().Replace(@"font24"">", "").Replace("<", "");
            string vdead = listData[3].ToString().Replace(@"font24"">", "").Replace("<", "");
            string wcase = listData[4].ToString().Replace(@"font24"">", "").Replace("<", "");
            string wtreatment = listData[5].ToString().Replace(@"font24"">", "").Replace("<", "");
            string wfine = listData[6].ToString().Replace(@"font24"">", "").Replace("<", "");
            string wdead = listData[7].ToString().Replace(@"font24"">", "").Replace("<", "");


            Application.Current.Dispatcher.Invoke(new Action(() => {
                vCase.Text = vcase;
                vTreatment.Text = vtreatment;
                vFine.Text = vfine;
                vDead.Text = vdead;
                wCase.Text = wcase;
                wTreatment.Text = wtreatment;
                wFine.Text = wfine;
                wDead.Text = wdead;
            }));


        }

        private void Info_Click(object sender, RoutedEventArgs e)
        {
            AppInfo appInfo = new AppInfo();
            appInfo.Show();
        }

        private void PassChange_GotFocus(object sender, RoutedEventArgs e)
        {
            ChangePass passChanged = new ChangePass();
            passChanged.Show();
        }

        private void Mail_GotFocus(object sender, RoutedEventArgs e)
        {   
            ChangeByMail byMail = new ChangeByMail();
            byMail.Show();
        }

        private void MainController_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
            }
        }
    }
}

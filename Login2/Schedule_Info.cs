﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login2
{
    public class Schedule_Info
    {
        private string sId;
        private string sName;
        private string sCredits;
        private string sGroup;
        private string sDay;
        private string sLesson;
        private string sTime;
        private string sClass;
        private string sLocation;
        private string sWeek;

        public string SId { get => sId; set => sId = value; }
        public string SName { get => sName; set => sName = value; }
        public string SCredits { get => sCredits; set => sCredits = value; }
        public string SGroup { get => sGroup; set => sGroup = value; }
        public string SDay { get => sDay; set => sDay = value; }
        public string SLesson { get => sLesson; set => sLesson = value; }
        public string STime { get => sTime; set => sTime = value; }
        public string SClass { get => sClass; set => sClass = value; }
        public string SLocation { get => sLocation; set => sLocation = value; }
        public string SWeek { get => sWeek; set => sWeek = value; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using xNet;


namespace Login2
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        static string userName;
        static string passWord;
        public MainWindow()
        {
            InitializeComponent();
           
        }
    

        private void Login_Click(object sender, RoutedEventArgs e)
        {

            userName = email.Text;
            passWord = pass.Password.ToString();

            #region Request login

            #region add cookie
            HttpRequest httpLogin = new HttpRequest();
            httpLogin.Cookies = new CookieDictionary();
            var cookies = httpLogin.Get("https://sso.hcmut.edu.vn/cas/login").Cookies;
            httpLogin.Cookies.Add("_ga", "GA1.3.1366266226.1612606482");
            #endregion

            #region Post Login
            string html = httpLogin.Get("https://sso.hcmut.edu.vn/cas/login").ToString();
            string pattern = @"LT-(.*?)""";
            string tokenLT = Regex.Match(html, pattern, RegexOptions.Singleline).Value.Replace(@"""", "");
            string data = @"username="+userName+"&password="+passWord+"&lt=" + tokenLT + "&execution=e2s1&_eventId=submit&submit=Login";
            html = httpLogin.Post("https://sso.hcmut.edu.vn/cas/login?service=http%3A%2F%2Fmybk.hcmut.edu.vn%2Fstinfo%2F", data, "application/x-www-form-urlencoded").ToString();
            #endregion

            #endregion


            var error = html.IndexOf("Log In Successful");
            if (error == -1)
                {
                    MessageBox.Show("Sai tài khoản hoặc mật khẩu");

                }

            else
            {

                Waiting waitingwindow = new Waiting();
                waitingwindow.Show();

                #region Getdata

                //chromeDriver.Url = "https://mybk.hcmut.edu.vn/stinfo/";

               string htmlGet= httpLogin.Get("https://mybk.hcmut.edu.vn/stinfo/").ToString();

                string name = "";
                string group = "";

                Task TagGroup = new Task(() =>
                {
                    try
                    {
                        pattern = @"dropdown(.*?)</p";
                        string nameListReg = Regex.Match(htmlGet, pattern, RegexOptions.Singleline).Value.Replace(@"dropdown"">", "").Replace(@"</div>", "").Replace("</", "").Trim();
                        nameListReg = nameListReg.Replace(@"
                    
                    ", "");
                        nameListReg = System.Net.WebUtility.HtmlDecode(nameListReg);         // convert web to unicode format
                        int pos = nameListReg.IndexOf('<');
                        name = nameListReg.Substring(0, pos);

                        pos = nameListReg.IndexOf(">");
                        group = nameListReg.Remove(0, pos);
                        group = group.Remove(group.Length - 1, 1);
                        group = group.Remove(0, 1);
                    }
                    catch { }

                   
                  
                });

                TagGroup.Start();

                #region Token
                pattern = @"token(.*?)>";
                string token = Regex.Match(htmlGet, pattern, RegexOptions.Singleline).Value;
                token = token.Replace(@"token"" content=""", "").Replace(@""" />", "");
                #endregion


                #region Student Data
                var testSchedule = httpLogin.Post("https://mybk.hcmut.edu.vn/stinfo/lichthi/ajax_lichthi", "_token=" + token, "application/x-www-form-urlencoded; charset=UTF-8").ToString();
                var schedule = httpLogin.Post("https://mybk.hcmut.edu.vn/stinfo/lichthi/ajax_lichhoc", "_token=" + token, "application/x-www-form-urlencoded; charset=UTF-8").ToString();
                var score = httpLogin.Post("https://mybk.hcmut.edu.vn/stinfo/grade/ajax_grade", "_token=" + token, "application/x-www-form-urlencoded; charset=UTF-8").ToString();
                #endregion

                // hiden textBlock
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    tkb.Text = schedule;
                    diem.Text = score;
                    thi.Text = testSchedule;
                    iname.Text = name;
                    igroup.Text = group;
                }));

                MainControl mainControl = new MainControl();

                mainControl.Show();

                // passing value 
                Schedule scheduleWindow = new Schedule();
                TestScore testScoreWindow = new TestScore();
                Exam examWindow = new Exam();

                waitingwindow.Close();

                #endregion
                Close();

            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

      

        private void MainWin_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login2
{
    public class CourseInfo
    {
        private string courseName;
        private string url;
        private string userName;
        private string passWord;

        public string CourseName { get => courseName; set => courseName = value; }
        public string Url { get => url; set => url = value; }
        public string UserName { get => userName; set => userName = value; }
        public string PassWord { get => passWord; set => passWord = value; }
    }
}

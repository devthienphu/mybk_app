﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login2
{
    public class TestScore_Info
    {
        private string tId;
        private string tName;
        private string tGroup;
        private string tCredits;
        private string tScore;
        private string tExam;
        private string tFinal;
        private string tSem;

        public string TId { get => tId; set => tId = value; }
        public string TName { get => tName; set => tName = value; }
        public string TGroup { get => tGroup; set => tGroup = value; }
        public string TCredits { get => tCredits; set => tCredits = value; }
        public string TScore { get => tScore; set => tScore = value; }
        public string TExam { get => tExam; set => tExam = value; }
        public string TFinal { get => tFinal; set => tFinal = value; }
        public string TSem { get => tSem; set => tSem = value; }
    }
}

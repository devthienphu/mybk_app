﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Login2
{
    /// <summary>
    /// Interaction logic for TestScore.xaml
    /// </summary>
    /// 
    public partial class TestScore : Window
    {
        ObservableCollection<TestScore_Info> info;

        static string scoreJson = ((MainWindow) Application.Current.MainWindow).diem.Text;
        static int count = 0;

        public ObservableCollection<TestScore_Info> Info { get => info; set => info = value; }

        public TestScore()
        {
            InitializeComponent();
            Info = new ObservableCollection<TestScore_Info>();

            count++;
            if (count > 1)
            {
                Task thrdScore = new Task(Getdata);
                thrdScore.Start();
            }
           
        }


        void Getdata()
        {
            string json = scoreJson;
            JObject jsConvert = JObject.Parse(json);

            string scoreList = "{"+ jsConvert.First.ToString()+"}";

            JObject ScoreList = JObject.Parse(scoreList);

            int Count = ScoreList.First.First.First.First.Count(); 

            for(int i = 0; i < Count; i++)
            {
                string score = ScoreList.First.First.First.First[i].ToString();
                JObject Score = JObject.Parse(score);

                string tId = Score["ma_mh"].ToString();
                string tName = Score["ten_mh"].ToString();
                string tGroup = Score["nhomto"].ToString();
                string tCredits = Score["so_tin_chi"].ToString();
                string tScore = Score["diem_thanhphan"].ToString();
                string tExam = Score["diem_thi"].ToString();
                string tFinal = Score["diem_tong_ket"].ToString();
                string tSem = Score["ten_hocky"].ToString();
                


                Application.Current.Dispatcher.Invoke(new Action(() => {
                    Info.Add(new TestScore_Info
                    {
                        TId = tId,
                        TName=tName,
                        TGroup= tGroup,
                        TCredits=tCredits,
                        TScore=tScore,
                        TExam=tExam,
                        TFinal=tFinal,
                        TSem=tSem
                    });

                    Semester.Text = tSem;

                    listScore.ItemsSource = Info;
                }));


            }

            // JObject Scores = JObject.Parse(scores);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}

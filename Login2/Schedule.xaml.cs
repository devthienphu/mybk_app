﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Login2
{
    /// <summary>
    /// Interaction logic for Schedule.xaml
    /// </summary>
    public partial class Schedule : Window
    {


        ObservableCollection<Schedule_Info> sInfo;

        static string JsonSchedule = ((MainWindow)Application.Current.MainWindow).tkb.Text;
        static int count =0;

        public ObservableCollection<Schedule_Info> SInfo { get => sInfo; set => sInfo = value; }

        //static string JsonScore = ((MainWindow)Application.Current.MainWindow).diem.Text;
        //static string JsonTest = ((MainWindow)Application.Current.MainWindow).thi.Text;

        public Schedule()
        {
            InitializeComponent();
            count++;
            SInfo = new ObservableCollection<Schedule_Info>();

            if (count > 1)
            {
                Task Sthrd = new Task(Getdata);
                Sthrd.Start();
            }   
        }

       void Getdata()
        {

            string temp = JsonSchedule;
         
            temp = @"{""course"":" + temp + "}";
            JObject Jsontemp = JObject.Parse(temp);

            string tempJson = Jsontemp.First.First[0].ToString();       // get newest course list

            JObject schedulesJs = JObject.Parse(tempJson);

            int count = schedulesJs.First.First.Count();

            for(int i = 0; i < count; i++)
            {
                string scheduleJs = schedulesJs.First.First[i].ToString(0);

                JObject subject = JObject.Parse(scheduleJs);
                string sId = subject["ma_mh"].ToString();
                string sName = subject["ten_mh"].ToString();
                string sCredits = subject["so_tin_chi"].ToString();
                string sGroup = subject["nhomto"].ToString();
                string sDay = subject["thu1"].ToString();
                string sLession = subject["tiet_bd1"].ToString() + "-" + subject["tiet_kt1"].ToString();
                string sTime = subject["giobd"].ToString() + "-" + subject["giokt"].ToString();
                string sClass = subject["phong1"].ToString();
                string sLocation = subject["macoso"].ToString();
                string sWeek = subject["tuan_hoc"].ToString();
                string sem = subject["ten_hocky"].ToString();
                

                Application.Current.Dispatcher.Invoke(new Action(() => {
                    SInfo.Add(new Schedule_Info
                    {
                        SId = sId,
                        SName = sName,
                        SCredits = sCredits,
                        SClass = sClass,
                        SGroup = sGroup,
                        SDay = sDay,
                        SLesson = sLession,
                        STime = sTime,
                        SLocation = sLocation,
                        SWeek = sWeek
                    });
                    Semester.Text = sem;
                    listSubject.ItemsSource = SInfo;
                }));
            }
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}

﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Threading;

namespace Login2
{
    /// <summary>
    /// Interaction logic for Exam.xaml
    /// </summary>
    public partial class Exam : Window
    {
        #region Method
        ObservableCollection<ExamInfo> info;
        static string examJson = ((MainWindow)Application.Current.MainWindow).thi.Text;
        static int count = 0;

        public ObservableCollection<ExamInfo> Info { get => info; set => info = value; }

        #endregion
        public Exam()
        {
            InitializeComponent();
            Info = new ObservableCollection<ExamInfo>();
            count++;
            if (count > 1)
            {
               Task thrdExam = new Task(Getdata);
                thrdExam.Start();
            }

        }

        void Getdata()
        {
            JObject jsConvert = JObject.Parse(examJson);

            string examList = @"{""thi"":" + jsConvert.First.First.First.First.ToString() +"}";

            JObject examsListJs = JObject.Parse(examList);

            int Count = examsListJs.First.First.Count();

            for(int i = 0; i < Count; i++)
            {
                string exam = examsListJs.First.First[i].ToString();
                JObject examConvert = JObject.Parse(exam);

                string eId = examConvert["ma_mh"].ToString();
                string eName = examConvert["ten_mh"].ToString();
                string eGroup = examConvert["nhomto"].ToString();
                string eDayGK = examConvert["ngaykt"].ToString();
                string eTimeGK = examConvert["gio_kt"].ToString();
                string eRoomGK = examConvert["phong_ktra"].ToString();
                string eDayCK = examConvert["ngaythi"].ToString();
                string eTimeCK = examConvert["gio_thi"].ToString();
                string eRoomCK = examConvert["phong_thi"].ToString();
                
                Application.Current.Dispatcher.Invoke(new Action(() => {
                    Info.Add(new ExamInfo
                    {
                        EID = eId,
                        EName = eName,
                        EGroup = eGroup,
                        EDayGK = eDayGK,
                        ETimeGK = eTimeGK,
                        ERoomGK = eRoomGK,
                        EDayCK = eDayCK,
                        ETimeCK = eTimeCK,
                        ERoomCK = eRoomCK
                    });
                    semester.Text = examConvert["ten_hk_nh"].ToString();
                    listTest.ItemsSource = Info;
                }));


            }



        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ExamControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}

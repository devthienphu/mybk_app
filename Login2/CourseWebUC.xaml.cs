﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CefSharp;

namespace Login2
{
    /// <summary>
    /// Interaction logic for CourseWebUC.xaml
    /// </summary>
    public partial class CourseWebUC : UserControl, INotifyPropertyChanged
    {
        private CourseInfo courseUrl;
        public CourseInfo CourseUrl { get { return courseUrl; } set { courseUrl = value; OnPropertyChanged("CourseUrl"); } }

        static int count = 0;

        public CourseWebUC()
        {
            InitializeComponent();
            this.DataContext = CourseUrl;

            browser.LoadingStateChanged += (sender, args) =>
            {

                if (args.IsLoading == false)
                {
                    string script_btn = @"document.querySelector(""#region-main>div>div.row>div>div>div>div.row>div>div>div:nth-child(1)>a"").click();";
                    string script_user = @"document.getElementById('username').value='" + courseUrl.UserName + "'";
                    string script_pass = @"document.getElementById('password').value='" + courseUrl.PassWord + "'";
                    string script_click= @"document.querySelector(""#fm1>div.row.btn-row>input.btn-submit"").click();";
                    browser.EvaluateScriptAsync(script_btn);
                    browser.EvaluateScriptAsync(script_user);
                    browser.EvaluateScriptAsync(script_pass);
                    browser.EvaluateScriptAsync(script_click);
                    
                }
            };     

        }




        public event EventHandler backToMain;       // raise event backToMain


        public event EventHandler BackToMain
        {
            add  { backToMain += value; }
            remove { backToMain -= value; }
        }
        private void Btn_Back_Click(object sender, RoutedEventArgs e)
        {
            if (backToMain != null)
                backToMain(this, new EventArgs());

        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string newName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(newName));
            }
        }

        private void Btn_View_Click(object sender, RoutedEventArgs e)
        {
            if (courseUrl != null) { 
                    browser.Load(courseUrl.Url);
            }
            count++;
               
        }

        private void BackWeb_Click(object sender, RoutedEventArgs e)
        {
            if (browser.CanGoBack)
            {
                browser.Back();
            }
        }

        private void ForwardWeb_Click(object sender, RoutedEventArgs e)
        {            
            if (browser.CanGoForward)
            {
                browser.Forward();
            }
        }
    }

    
}
